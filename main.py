import uvicorn
import os
import requests
from typing import Union
from fastapi import FastAPI

app = FastAPI()


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Union[str, None] = None):
    return {"item_id": item_id, "q": q}

@app.get("/ingredients/")
def get_info_produit():
    api_url = "https://world.openfoodfacts.org/api/v0/product/3256540001305.json"
    response = requests.get(api_url)
    res = response.json()
    if response.status_code != 200 or res == []:
        return None
    return(res["product"]["ingredients"])

@app.get("/ingredients/vegan/")
def is_vegan():
    ingredients = get_info_produit()
    for i in range(len(ingredients)):
        if ingredients[i]["vegan"] in ["maybe", "no"]:
            return False
    return True

if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000)