# TP5 Conception de logiciel

## Installation 

Commencez par installer les packages necessaires au bon fonctionnement de l'application. 
Ils se trouvent donc dans le fichier requirements.txt. 
Dans votre terminal, écrivez puis compilez la ligne suivante. 
```
pip install -r requirements.txt
```

## Lancement de l'APIs
Ensuite, lancer le fichier main.py avec la commande suivante: 
```
python main.py
```
Ouvrez une nouvelle page et entrez l'URL indiqué : 
```
http://127.0.0.1:8000
```
L'API est maintenant lancée. 